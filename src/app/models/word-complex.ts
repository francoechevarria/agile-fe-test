export class WordComplexModel {
  id: number;
  text: string;
  bold: boolean = false;
  underline: boolean = false;
  italic: boolean = false;
  color: string = '#000000';
}

