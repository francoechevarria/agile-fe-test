import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import {TextService} from '../text-service/text.service';
import {WordComplexModel} from '../models/word-complex';
import {HelperService} from '../helper-service/helper.service';


/**
 * This component will return in the current selected text in the dom the new formatted html
 */

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {

  optionsList: any;
  showSynonyms = false;
  loadingSynonyms = true;

  @Input() currentSelectedWord: WordComplexModel;
  @Input() currentArray: WordComplexModel[];
  @Input() templateType: string = 'BIG';

  constructor(private textService: TextService, private changeDetection: ChangeDetectorRef, private helperService: HelperService) {

    // subscribe to  changes in event emmitter for new word selected
    this.helperService.wordArrayObs$.subscribe((data) => {
      this.closeSynonyms();
      this.currentSelectedWord = data.word;
      this.currentArray = data.arrayOfwords;
      this.changeDetection.detectChanges();
    });
  }


  /**
   * Toggle the bold style
   */
  toggleBold() {
    if (this.currentSelectedWord) {
      this.closeSynonyms();
      this.optionsList = [];
      this.currentSelectedWord.bold = !this.currentSelectedWord.bold;
      this.replaceInArrayOfWords();
    }
  }


  /**
   * Toggle the italic style
   */
  toggleItalic() {
    if (this.currentSelectedWord) {
      this.closeSynonyms();
      this.optionsList = [];
      this.currentSelectedWord.italic = !this.currentSelectedWord.italic;
      this.replaceInArrayOfWords();
    }
  }

  /**
   * Toggle the underline style
   */
  toggleUnderline() {
    if (this.currentSelectedWord) {
      this.closeSynonyms();
      this.optionsList = [];
      this.currentSelectedWord.underline = !this.currentSelectedWord.underline;
      this.replaceInArrayOfWords();
    }
  }


  /**
   * Get the list of synonyms for a word and display a list
   */
  getSynonyms() {
    this.optionsList = [];
    this.loadingSynonyms = true;
    if (this.currentSelectedWord !== undefined) {
      this.showSynonyms = true;
      this.textService.getSynonyms(this.currentSelectedWord.text).subscribe((data) => {
        this.optionsList = [];
        this.optionsList = data;
        this.showSynonyms = true;
        this.loadingSynonyms = false;
        this.changeDetection.detectChanges();
      }, error1 => {
        this.optionsList = null;
        this.loadingSynonyms = false;
        this.showSynonyms = false;
      });
    }
  }

  /**
   * Change color, update the word and update
   * @param e
   */
  changeColor(e) {
    this.currentSelectedWord.color = e;
    this.replaceInArrayOfWords();
  }

  /**
   * Hide the synonymous list
   */
  closeSynonyms() {
    this.showSynonyms = false;
  }

  /**
   * Replace the current word in the array and emit a event to notify the differents instances of the components.
   */
  replaceInArrayOfWords() {
    const index = this.currentArray.findIndex(item => item.id === this.currentSelectedWord.id);
    this.currentArray.splice(index, 1, this.currentSelectedWord);
    this.helperService.emitArrayOfWordsChange(this.currentArray, this.currentSelectedWord);
  }

  /**
   * Replace the word with a synonym, update and close the list of synonym.
   * @param {string} text
   */
  selectSynonym(text: string) {
    this.currentSelectedWord.text = text;
    this.replaceInArrayOfWords();
    this.closeSynonyms();
  }


}
