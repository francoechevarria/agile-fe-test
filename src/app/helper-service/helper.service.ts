import { Injectable } from '@angular/core';
import {Subject} from "rxjs/internal/Subject";
import {WordComplexModel} from "../models/word-complex";

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  private changedWordsArraySource = new Subject<{arrayOfwords: WordComplexModel[], word: WordComplexModel}>();
  public wordArrayObs$ = this.changedWordsArraySource.asObservable();


  constructor() { }

  /**
   *  Use to comunication between the instances of the Control Panel component
   * @param {WordComplexModel[]} value
   * @param {WordComplexModel} word
   */
  public emitArrayOfWordsChange(value: WordComplexModel[], word: WordComplexModel) {
    this.changedWordsArraySource.next({arrayOfwords: value, word: word});
  }
}
