import {ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {WordComplexModel} from '../models/word-complex';
import {HelperService} from '../helper-service/helper.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileComponent {
  textOriginal: string = ``;
  arrayOfWords: WordComplexModel[] = [];
  showSmallControlPanel = false;
  selectedWord: WordComplexModel;


  constructor(private changeDetection: ChangeDetectorRef, private helperService: HelperService) {
    this.helperService.wordArrayObs$.subscribe((data) => {
      this.selectedWord = data.word;
      this.arrayOfWords = data.arrayOfwords;
      this.changeDetection.detectChanges();
    });
  }

  /**
   * Split the new text in a new array
   */
  splitWords() {
    this.arrayOfWords = [];
    const splittedArray = this.textOriginal.split(' ');
    for (var i = 0; i < splittedArray.length; i++) {
      const element = splittedArray[i];
      const newWord: WordComplexModel = new WordComplexModel();
      newWord.id = i + 1;
      newWord.text = element;
      this.arrayOfWords.push(newWord);
    }
  }


  /**
   * New word selected. Emit and notify to components
   * @param {WordComplexModel} word
   */
  wordSelected(word: WordComplexModel) {
    if (this.selectedWord && word.id === this.selectedWord.id) {
      this.selectedWord = null;
      this.showSmallControlPanel = true;
      this.helperService.emitArrayOfWordsChange(this.arrayOfWords, null);
    } else {
      this.showSmallControlPanel = true;
      this.selectedWord = word;
      this.helperService.emitArrayOfWordsChange(this.arrayOfWords, this.selectedWord);
    }

  }




}
