import {Injectable} from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {catchError, map} from "rxjs/operators";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {throwError} from "rxjs/internal/observable/throwError";

@Injectable()
export class TextService {

  constructor(private http: HttpClient) {
  }

  /**
   * Return the list of synonyms from the datamuse API
   * @param {string} word
   * @returns {Observable<{} | any>}
   */
  getSynonyms(word: string): Observable<{} | any> {
    const url: string = `https://api.datamuse.com/words?rel_syn=${word}`;
    return this.http.get(url).pipe(
      map(this.extractSynonymsResponse.bind(this)),
      catchError((err: HttpErrorResponse) => {
        console.log('ERROR: ', err.statusText);
        return throwError(err);
      })
  )
    ;
  }

  extractSynonymsResponse(res: any) {
    return res;
  }
}
